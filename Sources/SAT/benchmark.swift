//
//  benchmark.swift
//  Run
//
//  Created by Toni Kocjan on 23/03/2020.
//

import Foundation

public func benchmark(_ run: () throws -> Void) rethrows -> Double {
  let current = time()
  try run()
  return time() - current
}

public func benchmark<T>(_ run: () throws -> T) rethrows -> (time: Double, value: T) {
  let current = time()
  let value = try run()
  return (time() - current, value)
}

func time() -> Double {
  #if os(macOS)
  return CFAbsoluteTimeGetCurrent()
  #else
  return Date().timeIntervalSinceNow
  #endif
}
