//
//  DIMACS.swift
//  SAT
//
//  Created by Toni Kocjan on 20/03/2020.
//

import Foundation

public typealias Dimacs = String

public func generateDIMACS<T>(_ expression: Expression<T>, n: Int) -> Dimacs {
  switch expression {
  case .and(let expressions):
    let dimacs = "p cnf \(n) \(expressions.count)\n"
    return dimacs + expressions.map(DIMACS).joined(separator: " 0\n") + " 0"
  case _:
    fatalError("Root node must be `and`!")
  }
}

func generateDIMACS<T>(_ expression: Expression<T>) -> Dimacs {
  switch expression {
  case .and(let expressions):
    return expressions.map(DIMACS).joined(separator: " 0\n") + " 0"
  case _:
    fatalError("Root node must be `and`!")
  }
}

func DIMACS<T>(_ e: Expression<T>) -> String {
  switch e {
  case .or(let expressions):
    return expressions.map(DIMACS).joined(separator: " ")
  case .negate(let expression):
    return "-" + DIMACS(expression)
  case .variable(let value):
    return String(describing: value)
  case .and(let expressions):
    return String(generateDIMACS(.and(expressions)).dropLast(2))
  }
}

