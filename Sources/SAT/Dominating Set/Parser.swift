//
//  Parser.swift
//  SAT
//
//  Created by Toni Kocjan on 20/03/2020.
//

import Foundation

public enum DimacsParserError: Error, CustomStringConvertible {
  case invalidPath(String)
  
  public var description: String {
    switch self {
    case .invalidPath(let path):
      return """
      Could not open file `\(path)`.
      """
    }
  }
}

public func dimacsGraphParser(filePath path: String) throws -> Graph<Int> {
  do {
    let text = try String(contentsOfFile: path, encoding: .utf8)
    return parseText(text)
  } catch {
    throw DimacsParserError.invalidPath(path)
  }
}

func parseText(_ text: String) -> Graph<Int> {
  let edges: [(Int, Int)] = text
    .split(separator: "\n")
    .filter { $0.starts(with: "e ") }
    .map { $0.dropFirst(2) }
    .map { $0.split(separator: " ") }
    .map { (Int($0[0])!, to: Int($0[1])!) }
  return .init(edges: edges, isDirected: false)
}
