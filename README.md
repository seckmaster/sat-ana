# 1st assignment (ANA)

## 1. N-queens problem

Translate the n-queens problem into SAT. To solve SAT, we use [cryptominisat](https://github.com/msoos/cryptominisat) which accepts `cnf` as an input in the [`DIMACS`](http://www.satcompetition.org/2009/format-benchmarks2009.html) format.

[Implementation](https://gitlab.com/seckmaster/sat-ana/-/blob/master/Sources/SAT/Queens/NQueens.swift).

## 2. Dominating set problem

Similarily, translate (reduce) optimization problem of [`dominating set`](https://en.wikipedia.org/wiki/Dominating_set)(DS) into a decision problem, and then into SAT:

```
Given a graph G and a number k, answer with `yes` if graph G 
contains a dominating set of length k, `no` otherwise.
```

Provide solutions, optimal or sub-optimal, for input graphs (graphs are given in `DIMACS` format and can be found under `/Graphs`).


### 2.1. Reduction to SAT

We first begin by solving a simplified problem of finding a DS of an _arbitrary_ length. Reducing to SAT is trivial. Given a graph `G = (V, E)`, let `w(n), n ∊ V` be the set of neighbouring vertices of `n` - `∀m ∊ w(n), ∃e(n, m) ∊ E`.

To obtain a dominating set of graph G, we generate the following clauses:

  1. `∀n ∊ N, ∀m ∊ w(n); n ｖ m` (for each vertex, either the vertex is in DS or at least one of its neighbors). 

[Reduction 1]

We call this reduction a _baseline_ reduction and use it to obtain an upper-bound size of dominating sets for input graphs:

  1. `g1`: 52 vertices
  2. `g2`: 4 vertices
  3. `g3`: 23 vertices
  4. `g4`: 48 vertices
  5. `g5`: 7 vertices 

___

To solve the actual problem of checking whether a graph contains a DS of a _specific_ size `k`, we introduce additional variables and generate supplementary clauses:

  * Variables: `V𝗋,𝚒` for `𝟣 <= 𝗋 <= 𝗄, 𝟣 <= 𝚒 <= 𝚗` where n is number of vertices in graph. `V𝗋,𝚒` is `true` if vertex `𝚒` is `𝗋`th vertex in the DS. We might imagine variables as a `k * n` matrix where each column is a specific vertex of the graph and each row a sequential index.

  * Clauses:
    1. A node cannot be both `l`th and `k`th vertex in DS. In other words, only one variable is allowed to be `true` vertically.

    
    2. Each sequential index `𝚒, 𝟣 <= 𝚒 <= 𝚗` can only contain one vertex. In other words, only one variable is allowed to be `true` horizontally.

    
    3. Same clauses as in [Reduction 1] except that they have to be generated for all rows of the variable matrix. 
 
 [Reduction 2]
 
Using [Reduction 2] we try to find optimal dominating sets or at least a lower - upper bound range. Compared to [Reduction 1], we found this improvements:

  1. `g1`: between 10 - 52 vertices
  2. `g2`: 3 vertices (optimal solution)
  3. `g3`: between 6 -  16 vertices
  4. `g4`: between 7 - 48 vertices
  5. `g5`: between 4 - 5 vertices 

[Implementation](https://gitlab.com/seckmaster/sat-ana/-/blob/master/Sources/SAT/Dominating%20Set/DominatingSet.swift).

## Building and running the project

1. Clone the repository: 

	```bash
	$ git clone https://gitlab.com/seckmaster/sat-ana.git
	```

2. Make sure Swift 5.1 (or 5.2) is installed:

	```bash
	$ swift --version
	  Apple Swift version 5.1.3 (swiftlang-1100.0.282.1 clang-1100.0.33.15)
	  Target: x86_64-apple-darwin19.3.0
	```

   or

	```bash
	  Swift version 5.1.5 (swift-5.1.5-RELEASE)
	  Target: x86_64-unknown-linux-gnu
	```


	If you don't have Swift installed follow [Linux installation guidlines](https://itsfoss.com/use-swift-linux/).

3. Build and run:

	```bash
	$ cd sat-ana/
	$ swift run -c release Run
	
	OVERVIEW: SAT solver!

	USAGE: solver <subcommand>
	
	OPTIONS:
	  -h, --help              Show help information.
	
	SUBCOMMANDS:
	  queens                  
	  dominating_set 
	```
4. Usage:

	4.1. N-queens:
	
	```bash
	$ swift run -c release Run queens -n 8 -vs
   ✅ | generated constraints
   ✅ | generated DIMACS
   ✅ | solved problem
   ➕  ➕  👑  ➕

   👑  ➕  ➕  ➕

   ➕  ➕  ➕  👑

   ➕  👑  ➕  ➕
   ℹ️ | executed in 0.07s.
   
   $ swift run -c release Run queens -n 2 -vs                         
   ✅ | generated constraints
   ✅ | generated DIMACS
   ❌ | SAT is not satisfiable!
   ℹ️ | executed in 0.08s.
	```
	4.2. Dominating set
	
	```bash
    $ swift run -c release Run dominating_set -f Graphs/g3.txt -v
    ✅ | parsed graph with 125 nodes and 1472 edges
    ✅ | generated constraints
    ✅ | generated DIMACS
    ✅ | solved problem with 23 nodes
    ✅ | solution is a valid dominating set
    ℹ️ | executed in 0.09s.
    
    $ swift run -c release Run dominating_set -f Graphs/g3.txt -v -k 20
    ✅ | parsed graph with 125 nodes and 1472 edges
    ✅ | generated constraints
    ✅ | generated DIMACS
    ✅ | solved problem with 20 nodes
    ✅ | solution is a valid dominating set
    ℹ️ | executed in 29.97s.
    
    swift run -c release Run dominating_set -f Graphs/g3.txt -v -k 3 
    ✅ | parsed graph with 125 nodes and 1472 edges
    ✅ | generated constraints
    ✅ | generated DIMACS
    ❌ | SAT is not satisfiable!
    ℹ️ | executed in 0.52s.
	```
 
## References

[1] [k-clique to SAT reduction](https://blog.computationalcomplexity.org/2006/12/reductions-to-sat.html)
