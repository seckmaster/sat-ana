//
//  Expression.swift
//  SAT
//
//  Created by Toni Kocjan on 19/03/2020.
//

import Foundation

///
/// Intermediate structure representing logical expressions.
///
/// By representing SAT expressions like this we are able to analyse abd perform operations
/// on the tree after it is generated. 
/// But we sacrifice generation speed and memory!
///
public indirect enum Expression<T> {
  case variable(T)
  case and([Expression])
  case or([Expression])
  case negate(Expression)
}

// MARK: - Equatable
extension Expression: Equatable where T: Equatable {
}

// MARK: - Hashable
extension Expression: Hashable where T: Hashable {
}

// MARK: - CustomStringConvertible, CustomDebugStringConvertible, CustomReflectable
extension Expression: CustomStringConvertible, CustomDebugStringConvertible, CustomReflectable {
  public var description: String { _description(self) }
  
  private func _description(_ expr: Expression) -> String {
    switch expr {
    case .variable(let value):
      return String(describing: value)
    case .and(let expressions):
      return "( " + expressions.map(_description).joined(separator: " ∧ ")  + " )"
    case .or(let expressions):
      return "( " + expressions.map(_description).joined(separator: " ∨ ")  + " )"
    case .negate(let e):
      return "¬" + _description(e)
    }
  }
  
  public var debugDescription: String { description }
  
  public var customMirror: Mirror { Mirror(self, children: []) }
}

// MARK: - Functor
extension Expression {
  public func map<U>(_ transform: (T) throws -> U) rethrows -> Expression<U> {
    switch self {
    case .and(let expressions):
      return .and(try expressions.map { try $0.map(transform) })
    case .or(let expressions):
      return .or(try expressions.map { try $0.map(transform) })
    case .negate(let expression):
      return try .negate(expression.map(transform))
    case .variable(let value):
      return .variable(try transform(value))
    }
  }
}
