//
//  NQueens.swift
//  SAT
//
//  Created by Toni Kocjan on 19/03/2020.
//

public func reduceNQueens(n: Int) -> Expression<(Int, Int)> {
  typealias E = Expression<(Int, Int)>
  
  /// generate conditions such that each row will contain **at least** one queen
  let rows: [E] =
    (0..<n).map { i in
      .or((0..<n).map { .variable((i, $0)) })
  }
  
  /// generate constraints such that each row will contain **exactly** one queen
  let rowConstraints: [E] =
    (0..<n).flatMap { (i: Int) -> [E] in
      (0..<n - 1).flatMap { (j: Int) -> [E] in
        (j + 1..<n).map { (k: Int) -> E in
          .or([.negate(.variable((i, j))), .negate(.variable((i, k)))])
        }
      }
  }
  
  /// generate constraints such that each column will contain **exactly** one queen
  let columnConstraints: [E] =
    (0..<n).flatMap { (i: Int) -> [E] in
      (0..<n - 1).flatMap { (j: Int) -> [E] in
        (j + 1..<n).map { (k: Int) -> E in
          .or([.negate(.variable((j, i))), .negate(.variable((k, i)))])
        }
      }
  }
  
  /// generate constraints such that each (left->right) diagonal will contain **exactly** one queen
  let diagConstraints1: [E] =
    (0..<n - 1).flatMap { (i: Int) -> [E] in
      (0..<n - i).flatMap { (j: Int) -> [E] in
        (j + 1..<n - i).flatMap { (k: Int) -> [E] in
          if i == 0 {
            return [.or([.negate(.variable((j + i, j))), .negate(.variable((k + i, k)))])]
          } else {
            return [.or([.negate(.variable((j, j + i))), .negate(.variable((k, k + i)))]),
                    .or([.negate(.variable((j + i, j))), .negate(.variable((k + i, k)))])]
          }
        }
      }
  }
  
  /// generate constraints such that each (right->left) diagonal will contain **exactly** one queen
  let diagConstraints2: [E] =
    (0..<n - 1).flatMap { (i: Int) -> [E] in
      (0..<n - i).flatMap { (j: Int) -> [E] in
        (j + 1..<n - i).flatMap { (k: Int) -> [E] in
          if i == 0 {
            return [.or([.negate(.variable((j, n - j - i - 1))), .negate(.variable((k, (n - k - i - 1))))])]
          } else {
            return [.or([.negate(.variable((j, n - j - i - 1))), .negate(.variable((k, (n - k - i - 1))))]),
                    .or([.negate(.variable((j + i, n - j - 1))), .negate(.variable((k + i, (n - k - 1))))])]
          }
        }
      }
  }
  
  return .and(rows + rowConstraints + columnConstraints + diagConstraints1 + diagConstraints2)
}

public func renderSolution(_ solution: Set<Int>, n: Int) -> String {
  (0..<n).map { i in
    (0..<n)
      .map { solution.contains(i * n + $0 + 1) ?  "👑" : "➕"}
      .joined(separator: "  ")
  }.joined(separator: "\n\n")
}
