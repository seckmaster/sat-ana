//
//  DominatingSet.swift
//  Run
//
//  Created by Toni Kocjan on 20/03/2020.
//

import Foundation

public func reduceDominatingSet<T>(_ graph: Graph<T>) -> Expression<T> {
  /// For every node, make sure at least one of it's neighbours is inside D.
  /// We do that by using one condition:
  ///
  ///     // this one is not necessary
  ///     // V => (not N1 and not N2 or ..., for every neighbour N of V)
  ///
  ///     not V => (N1 or N2 or ..., for every neighbour N of V)
  ///
  
  let constraints2: [Expression<T>] = graph.map { node, neighbours in
    .or([.variable(node)] + neighbours.map(Expression<T>.variable))
  }
  
  return .and(constraints2)
}

public func reduceDominatingSet(_ graph: Graph<Int>, limit k: Int) -> Expression<Int> {
  let n = graph.count
  
  // make sure each node is only once in D
  let constraints1: [Expression<Int>] = (1...n)
    .flatMap { i in
      (0..<k).flatMap { (j: Int) -> [Expression<Int>] in
        (j + 1..<k).map { l in
          .or([.negate(.variable(j * n + i)), .negate(.variable(l * n + i))])
        }
      }
  }
  // make sure that for each i in 0..<k only one node is in D
  let constraints3: [Expression<Int>] = (0..<k)
    .flatMap { (i: Int) -> [Expression<Int>] in
      (1...n - 1).flatMap { (j: Int) -> [Expression<Int>] in
        (j + 1...n).map { l in
          .or([.negate(.variable(i * n + j)), .negate(.variable(i * n + l))])
        }
      }
  }
  // make sure D is a valid DS
  let constraints4: [Expression<Int>] = graph
    .map { (node, neighbours) -> Expression<Int> in
      Expression.or((0..<k).flatMap { (i: Int) -> [Expression<Int>] in
        [Expression.variable(i * n + node)] + neighbours.map { Expression.variable(i * n + $0) }
      })
  }
  let and = Expression.and(constraints1 + constraints3 + constraints4)
  return and
}

public typealias DominatingSet = Set<Int>

/// Check whether `solution` is _valid_ for the given `graph`.
/// A solution is valid when very node of the graph can be visited
/// by traversing neighbouring nodes of the nodes in the dominating set.
public func evaluateSolution(_ solution: DominatingSet, graph: Graph<Int>) -> Bool {
  var visitedNodes = Set<Int>()
  for node in solution {
    let node_ = (node - 1) % graph.count + 1
    graph.neighbours(of: node_)!.forEach { visitedNodes.insert($0) }
    visitedNodes.insert(node_)
  }
  return visitedNodes.count == graph.count
}

/// Check whether `solution` is _optimal_.
/// We say that a solution is optimal when there is no node that can
/// be removed from the set such that dominating set is valid.
///
/// Note that this does not mean that `solution` is globaly optimal,
/// this function only checks whether this particular solution can be shortened.
public func isSolutionOptimizable(_ solution: DominatingSet, graph: Graph<Int>) -> [DominatingSet] {
  lookupTable = [:]
  return _isSolutionOptimizable(solution, graph: graph)
}

var lookupTable: [DominatingSet: [DominatingSet]] = [:]

func _isSolutionOptimizable(_ solution: DominatingSet, graph: Graph<Int>, depth: Int = 20) -> [DominatingSet] {
  if depth == 0 {
    return []
  }
  
  if let cached = lookupTable[solution] {
    print("from cache ...")
    return cached
  }
  
  var optimizations: [DominatingSet] = []
  
  let randomFactor = Int(max(7, log(Double(solution.count))))
  for _ in 0..<solution.count {
    let node = solution.randomElement()!
    
    var optimizedSolution = solution
    optimizedSolution.remove(node)
    
    guard evaluateSolution(optimizedSolution, graph: graph) else {
      print("Cannot optimize by removing \(node)")
      continue
    }
    
    let recursive = _isSolutionOptimizable(optimizedSolution,
                                           graph: graph,
                                           depth: depth - 1)
    
    let _optimizations: [DominatingSet]
    if !recursive.isEmpty {
      _optimizations = recursive.map { Set<Int>($0 + [node]) }
    } else {
      _optimizations = [.init([node])]
    }
    optimizations += _optimizations
    lookupTable[solution] = _optimizations
  }
  
  return optimizations
}
