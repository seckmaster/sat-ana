//
//  Solver.swift
//  Run
//
//  Created by Toni Kocjan on 21/03/2020.
//

import ArgumentParser
import Foundation
import SAT

struct Solver: ParsableCommand {
  static var configuration: CommandConfiguration = .init(
    abstract: "SAT solver!",
    subcommands: [Queens.self, DominatingSet.self]
  )
}

extension Solver {
  struct Queens: ParsableCommand, Verbosable {
    static var configuration: CommandConfiguration = .init(commandName: "queens")
    
    @Option(name: [.short], help: "Number of queens.")
    var n: Int
    
    @Flag(name: [.short, .long], help: "Print solution.")
    var solution: Bool
    
    @Flag(name: [.short, .long], help: "Print constraints in dimacs format.")
    var dimacs: Bool
    
    @Flag(name: [.short, .long], help: "Print constraints in cnf.")
    var constraints: Bool
    
    @Flag(name: [.short, .long], help: "Verbose mode.")
    var verbose: Bool
    
    func run() throws {
      let executionTime = benchmark {
        do {
          /// 1. Generate SAT conditions
          let constraints: Expression<Int> = reduceNQueens(n: n)
            .map { $0.0 * n + $0.1 }
            .map { $0 + 1 }
          
          logSuccess("generated constraints")
          
          /// 2. Convert to DIMACS format
          let dimacs: Dimacs = generateDIMACS(constraints, n: n)
          
          logSuccess("generated DIMACS")
          
          /// 3. Solve SAT problem
          let solved: Set<Int> = try solveSAT(inDimacsFormat: dimacs)
          
          logSuccess("solved problem")
          
          /// 4. Render solution
          if solution {
            print(renderSolution(solved, n: n))
          }
          
          if self.dimacs {
            print(dimacs)
          }
          
          if self.constraints {
            print(constraints)
          }
        } catch {
          logFailure(String(describing: error))
        }
      }
      logInfo(String(format: "executed in %.2fs.", executionTime))
    }
  }
}

extension Solver {
  struct DominatingSet: ParsableCommand, Verbosable {
    static var configuration: CommandConfiguration = .init(commandName: "dominating_set")
    
    @Option(name: [.short, .long])
    var fileName: String
    
    @Option(name: [.short])
    var k: Int?
    
    @Option(name: [.short, .long])
    var threads: Int?
    
    @Flag(name: [.short, .long], help: "Print solution.")
    var solution: Bool
    
    @Flag(name: [.short, .long], help: "Print constraints in dimacs format.")
    var dimacs: Bool
    
    @Flag(name: [.short, .long], help: "Print constraints in cnf.")
    var constraints: Bool
    
    @Flag(name: [.short, .long], help: "Verbose mode.")
    var verbose: Bool
    
    func run() throws {
      try solveProblem()
    }
  }
}

private extension Solver.DominatingSet {
  func solveProblem() throws {
    let executionTime = benchmark {
      do {
        let dimacs: Dimacs = try {
          /// 1. Generate graph
          let graph: Graph<Int> = try dimacsGraphParser(filePath: fileName)
          
          logSuccess("parsed graph with \(graph.count) nodes and \(graph.edgesCount) edges")
          
          /// 2. Generate constraints
          let constraints: Expression<Int>
          switch k {
          case let k?:
            constraints = reduceDominatingSet(graph, limit: k)
          case _:
            constraints = reduceDominatingSet(graph)
          }
          
          if self.constraints {
            print(constraints)
          }
          
          logSuccess("generated constraints")
          
          /// 3. Convert to DIMACS format
          let dimacs: Dimacs = generateDIMACS(constraints, n: graph.count)
          
          logSuccess("generated DIMACS")
          
          if self.dimacs {
            print(dimacs)
          }
          
          return dimacs
        }()
        
        /// 4. Solve SAT problem
        let solution: SAT.DominatingSet = try solveSAT(inDimacsFormat: dimacs, threads: threads)
        
        logSuccess("solved problem with \(solution.count) nodes")
        
        /// 5. Evaluate solution
        switch evaluateSolution(solution, graph: try dimacsGraphParser(filePath: fileName)) {
        case true:
          logSuccess("solution is a valid dominating set")
        case false:
          logFailure("solution is not a valid dominating set")
        }
        
        if self.solution {
          print(solution)
        }
        
//        optimize(solution, graph: graph)
      } catch {
        logFailure(String(describing: error))
      }
    }
    logInfo(String(format: "executed in %.2fs.", executionTime))
  }
  
  func optimize(_ solution: DominatingSet, graph: Graph<Int>) {
    for opt in isSolutionOptimizable(solution, graph: graph).sorted(by: { $0.count > $1.count }) {
      print(opt, opt.count)
    }
  }
}

protocol Verbosable {
  var verbose: Bool { get }
}

extension ParsableCommand where Self: Verbosable {
  func logSuccess(_ msg: String) {
    guard verbose else { return }
    print("✅ | \(msg)")
  }
  
  func logFailure(_ msg: String) {
    guard verbose else { return }
    print("❌ | \(msg)")
  }
  
  func logInfo(_ msg: String) {
    guard verbose else { return }
    print("ℹ️  | \(msg)")
  }
}
