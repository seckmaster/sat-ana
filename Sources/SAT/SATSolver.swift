//
//  SATSolver.swift
//  SAT
//
//  Created by Toni Kocjan on 20/03/2020.
//

import Foundation

public enum SATSolverError: Error, CustomStringConvertible {
  case unsatisfiable
  case minisatUnavailable(String)
  
  public var description: String {
    switch self {
    case .unsatisfiable:
      return "SAT is not satisfiable!"
    case .minisatUnavailable(let path):
      return """
      This program depends on `cryptominisat` utility:
        - https://github.com/msoos/cryptominisat
        - https://formulae.brew.sh/formula/cryptominisat
      Make sure it's installed correctly and that it's executable is located at `\(path)`.
      """
    }
  }
}

public var child: Process?

/// Solve SAT provided in `DIMACS` format.
/// https://people.sc.fsu.edu/~jburkardt/data/cnf/cnf.html
public func solveSAT(
  inDimacsFormat dimacs: Dimacs,
  cryptoMinisat: String = "/usr/local/bin/cryptominisat5",
  threads: Int? = nil) throws -> Set<Int>
{
  try isMinisatInstalled(cryptoMinisat)
  
  let tmpFile = NSTemporaryDirectory().appending("dimacs_cnf")
  saveToFile(dimacs, at: URL(fileURLWithPath: tmpFile))
  
  let task = Process()
  task.arguments = ["--verb", "0", tmpFile]
  threads.map { task.arguments!.append(contentsOf: ["-t", String($0)]) }
  task.currentDirectoryPath = "/"
  
  let outputPipe = Pipe()
  task.standardOutput = outputPipe
  
  task.launchPath = cryptoMinisat
  task.launch()
  child = task
  task.waitUntilExit()
  
  let stdout = String(data: outputPipe.fileHandleForReading.readDataToEndOfFile(),
                      encoding: .utf8)!
  
  if stdout.starts(with: "s SATISFIABLE") {
    let parsed = parseSatisfiable(stdout)
    return .init(parsed)
  }
  
  throw SATSolverError.unsatisfiable
}

func isMinisatInstalled(_ path: String) throws {
  if !FileManager.default.fileExists(atPath: path) {
    throw SATSolverError.minisatUnavailable(path)
  }
}

func saveToFile(_ string: String, at url: URL) {
  try! string.write(to: url, atomically: true, encoding: .utf8)
}

func parseSatisfiable(_ result: String) -> [Int] {
  result
    .split(separator: "\n")
    .dropFirst()
    .filter { $0.starts(with: "v") }
    .map { $0.dropFirst(2) }
    .joined(separator: " ")
    .split(separator: " ")
    .map { Int(String($0))! }
    .filter { (i: Int) -> Bool in i > 0 }
}
