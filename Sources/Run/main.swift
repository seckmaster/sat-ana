import Foundation
import Dispatch
import SAT

signal(SIGINT) {
  // need to manually kill child process (cryptominisat)
  child?.terminate()
  exit($0)
}
signal(SIGTERM) {
  // need to manually kill child process (cryptominisat)
  child?.terminate()
  exit($0)
}
Solver.main()
